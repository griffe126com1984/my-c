#ifndef _SORT_H
#define _SORT_H
#include "type.h"

typedef void (* TPFsoftFunc)(uint8* pArray, uint16 u16ArrSize);

typedef struct {
    uint8* pu8Array;
    uint16 u16ArrSize;
    TPFsoftFunc pfSortFunc;
} TSsortClass;
    
void SRT_BubbleSort(uint8* pu8Array, uint16 u16ArrSize);
void SRT_SelectSort(uint8* pu8Array, uint16 u16ArrSize);
void SRT_InsertSort(uint8* pu8Array, uint16 u16ArrSize);
void SRT_FastSort(uint8* pu8Array, uint16 u16ArrSize);

#endif