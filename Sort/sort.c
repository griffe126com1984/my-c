#include "sort.h"
#include "stdio.h"

void SRT_PrintArray(uint8 * pu8Array, uint16 u16ArrSize)
{
    uint16 u16Index;
    for (u16Index = 0; u16Index < u16ArrSize; u16Index++) {
        printf("%d,",pu8Array[u16Index]);
    }
    printf("\n------------------------------------------------------------\n");
}
void SRT_BubbleSort(uint8* pu8Array, uint16 u16ArrSize)
{
    uint16 u16Index=0;
    uint8 u8TempValue;
    SRT_PrintArray(pu8Array, u16ArrSize);
    if ((NULL == pu8Array) || (1 >= u16ArrSize)) {
        return;
    }
    for (u16Index = 0; u16Index < u16ArrSize-1; u16Index++) {
        if (pu8Array[u16Index] > pu8Array[u16Index+1]) {
            u8TempValue = pu8Array[u16Index];
            pu8Array[u16Index] = pu8Array[u16Index+1];
            pu8Array[u16Index+1] = u8TempValue;
        }
    }
    SRT_BubbleSort(pu8Array, u16ArrSize-1);
}

void SRT_SelectSort(uint8* pu8Array, uint16 u16ArrSize)
{
    uint16 u16Index = 0;
    uint16 u16MaxIndex = 0;
    uint8 u8TempValue = 0;

    SRT_PrintArray(pu8Array, u16ArrSize);

    if ((NULL == pu8Array) || (1 >= u16ArrSize)) {
        return;
    }
    u16MaxIndex = u16ArrSize-1;

    for (u16Index = u16MaxIndex; u16Index > 0; u16Index--) {
        if (pu8Array[u16Index-1] > pu8Array[u16MaxIndex]) {
            u16MaxIndex = u16Index-1;
        }
    }
    u8TempValue = pu8Array[u16MaxIndex];
    pu8Array[u16MaxIndex] = pu8Array[u16ArrSize-1];
    pu8Array[u16ArrSize-1] = u8TempValue;

    SRT_SelectSort(pu8Array, u16ArrSize-1);
}

void SRT_InsertSort(uint8* pu8Array, uint16 u16ArrSize)
{
    static uint16 u16InitSpacePos = 1;
    uint16 u16Index = 0;
    uint16 u16spaceIndex = 0;
    uint8 u8TempValue = 0;

    SRT_PrintArray(pu8Array, u16ArrSize);

    if ((NULL == pu8Array) || (1 >= u16ArrSize)) {
        return;
    }
    
    if (u16ArrSize == u16InitSpacePos) {
        u16InitSpacePos = 1;
        return;
    }

    u16spaceIndex = u16InitSpacePos;  

    u8TempValue = pu8Array[u16spaceIndex];

    while ((u16spaceIndex > 0) && (pu8Array[u16spaceIndex-1] > u8TempValue)) {
        pu8Array[u16spaceIndex] = pu8Array[u16spaceIndex-1];
        u16spaceIndex--;
    } 
    pu8Array[u16spaceIndex] = u8TempValue;
    
    u16InitSpacePos++;

    SRT_InsertSort(pu8Array, u16ArrSize);
}

void SRT_FastSort(uint8* pu8Array, uint16 u16ArrSize)
{
    uint16 leftIndex = 0;
    uint16 rightIndex = 0;
    uint8 u8Value = 0;
    uint8 u8TempValue = 0;

    SRT_PrintArray(pu8Array, u16ArrSize);

    if ((NULL == pu8Array) || (1 >= u16ArrSize)) {
        return;
    }

    rightIndex = u16ArrSize - 1;
    u8Value = pu8Array[rightIndex--];
    
    while(1) {
        while( pu8Array[leftIndex] < u8Value) {
            leftIndex++;
        }
        while((rightIndex >0) && (pu8Array[rightIndex] >= u8Value)) {
            rightIndex--;
        }
        if (leftIndex < rightIndex) {
            u8TempValue = pu8Array[leftIndex];
            pu8Array[leftIndex] = pu8Array[rightIndex];
            pu8Array[rightIndex] = u8TempValue;
        } else {
            break;
        }
    }

    pu8Array[u16ArrSize-1] = pu8Array[leftIndex];
    pu8Array[leftIndex] = u8Value;

    SRT_FastSort(pu8Array, leftIndex);
    if(leftIndex < (u16ArrSize-1)) {
        SRT_FastSort(&(pu8Array[leftIndex+1]), (u16ArrSize-1-leftIndex));
    }
}