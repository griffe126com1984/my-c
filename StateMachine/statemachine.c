#include "statemachine.h"

static StMac_TSfsmObj StMac_astFsms[StMac_NrOfFsm];

static uint8 StMac_Ly1_Off(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly1_Work(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly1_Shutdown(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly2_Init(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly2_Normal(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly2_Fault(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly3_Standby(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly3_Heating(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly3_Charge(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly3_Discharge(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly4_Charging(StMac_TSfsmWorkData * pstFsmWorkData);
static uint8 StMac_Ly4_Reduce(StMac_TSfsmWorkData * pstFsmWorkData);

static void StMac_ClearInEvents(StMac_TSfsmWorkData * pstFsmWorkData);
static void StMac_ClearOutEvents(StMac_TSfsmWorkData * pstFsmWorkData);
static void StMac_RunStateMachine(StMac_TSfsmObj * pstFsm, StMac_TSfsmWorkData * pstFsmWorkData);

static uint8 StMac_Ly1_Off(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly1State eState = StMac_Ly1_State_Off;

    if (TRUE == pstFsmWorkData->stInEvents.bLy1FrOffToWork) {
        eState                                    = StMac_Ly1_State_Work;
        pstFsmWorkData->stOutEvents.bEnterLy1Work = TRUE;
    }

    if (StMac_Ly1_State_Off != eState) {
        pstFsmWorkData->stOutEvents.bExitLy1Off = TRUE;
    }

    return (uint8)((StMac_Ly1_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly1_Work(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly1State eState = StMac_Ly1_State_Work;

    if (TRUE == pstFsmWorkData->stInEvents.bLy1FrWorkToShutdown) {
        eState                                        = StMac_Ly1_State_Shutdown;
        pstFsmWorkData->stOutEvents.bEnterLy1Shutdown = TRUE;
    }

    if (StMac_Ly1_State_Work != eState) {
        pstFsmWorkData->stOutEvents.bExitLy1Work = TRUE;
    }

    return (uint8)((StMac_Ly1_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly1_Shutdown(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly1State eState = StMac_Ly1_State_Shutdown;

    if (TRUE == pstFsmWorkData->stInEvents.bLy1FrShutdownToWork) {
        eState                                    = StMac_Ly1_State_Work;
        pstFsmWorkData->stOutEvents.bEnterLy1Work = TRUE;
    }

    if (StMac_Ly1_State_Shutdown != eState) {
        pstFsmWorkData->stOutEvents.bExitLy1Shutdown = TRUE;
    }

    return (uint8)((StMac_Ly1_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly2_Init(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly2State eState = StMac_Ly2_State_Init;

    if (TRUE == pstFsmWorkData->stInEvents.bLy2FrInitToFault) {
        eState                                     = StMac_Ly2_State_Fault;
        pstFsmWorkData->stOutEvents.bEnterLy2Fault = TRUE;
    } else if (TRUE == pstFsmWorkData->stInEvents.bLy2FrInitToNormal) {
        eState                                      = StMac_Ly2_State_Normal;
        pstFsmWorkData->stOutEvents.bEnterLy2Normal = TRUE;
    }

    if (StMac_Ly2_State_Init != eState) {
        pstFsmWorkData->stOutEvents.bExitLy2Init = TRUE;
    }

    return (uint8)((StMac_Ly2_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly2_Normal(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly2State eState = StMac_Ly2_State_Normal;

    if (TRUE == pstFsmWorkData->stInEvents.bLy2FrNormalToFault) {
        eState                                     = StMac_Ly2_State_Fault;
        pstFsmWorkData->stOutEvents.bEnterLy2Fault = TRUE;
    }

    if (StMac_Ly2_State_Normal != eState) {
        pstFsmWorkData->stOutEvents.bExitLy2Normal = TRUE;
    }

    return (uint8)((StMac_Ly2_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly2_Fault(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly2State eState = StMac_Ly2_State_Fault;

    if (TRUE == pstFsmWorkData->stInEvents.bLy2FrFaultToNormal) {
        eState                                      = StMac_Ly2_State_Normal;
        pstFsmWorkData->stOutEvents.bEnterLy2Normal = TRUE;
    }

    if (StMac_Ly2_State_Fault != eState) {
        pstFsmWorkData->stOutEvents.bExitLy2Fault = TRUE;
    }

    return (uint8)((StMac_Ly2_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly3_Standby(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly31State eState = StMac_Ly3_State_Standby;

    if (TRUE == pstFsmWorkData->stInEvents.bLy3FrStandbyToCharge) {
        eState                                      = StMac_Ly3_State_Charge;
        pstFsmWorkData->stOutEvents.bEnterLy3Charge = TRUE;
    } else if (TRUE == pstFsmWorkData->stInEvents.bLy3FrStandbyToHeating) {
        eState                                       = StMac_Ly3_State_Heating;
        pstFsmWorkData->stOutEvents.bEnterLy3Heating = TRUE;
    } else if (TRUE == pstFsmWorkData->stInEvents.bLy3FrStandbyToDisCharge) {
        eState                                         = StMac_Ly3_State_Discharge;
        pstFsmWorkData->stOutEvents.bEnterLy3Discharge = TRUE;
    }

    if (StMac_Ly3_State_Standby != eState) {
        pstFsmWorkData->stOutEvents.bExitLy3Standby = TRUE;
    }

    return (uint8)((StMac_Ly3_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly3_Heating(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly31State eState = StMac_Ly3_State_Heating;

    if (TRUE == pstFsmWorkData->stInEvents.bLy3FrHeatingToStandby) {
        eState                                       = StMac_Ly3_State_Standby;
        pstFsmWorkData->stOutEvents.bEnterLy3Standby = TRUE;
    }

    if (StMac_Ly3_State_Heating != eState) {
        pstFsmWorkData->stOutEvents.bExitLy3Heating = TRUE;
    }

    return (uint8)((StMac_Ly3_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly3_Charge(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly31State eState = StMac_Ly3_State_Charge;

    if (TRUE == pstFsmWorkData->stInEvents.bLy3FrChargeToStandby) {
        eState                                       = StMac_Ly3_State_Standby;
        pstFsmWorkData->stOutEvents.bEnterLy3Standby = TRUE;
    }

    if (StMac_Ly3_State_Charge != eState) {
        pstFsmWorkData->stOutEvents.bExitLy3Charge = TRUE;
    }

    return (uint8)((StMac_Ly3_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly3_Discharge(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly31State eState = StMac_Ly3_State_Discharge;

    if (TRUE == pstFsmWorkData->stInEvents.bLy3FrDisChargeToStandby) {
        eState                                       = StMac_Ly3_State_Standby;
        pstFsmWorkData->stOutEvents.bEnterLy3Standby = TRUE;
    }

    if (StMac_Ly3_State_Discharge != eState) {
        pstFsmWorkData->stOutEvents.bExitLy3Discharge = TRUE;
    }

    return (uint8)((StMac_Ly3_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly4_Charging(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly4State eState = StMac_Ly4_State_Charging;

    if (TRUE == pstFsmWorkData->stInEvents.bLy4FrChargingToReduce) {
        eState                                      = StMac_Ly4_State_Reduce;
        pstFsmWorkData->stOutEvents.bEnterLy4Reduce = TRUE;
    }

    if (StMac_Ly4_State_Charging != eState) {
        pstFsmWorkData->stOutEvents.bExitLy4Charging = TRUE;
    }

    return (uint8)((StMac_Ly4_NrOfState > eState) ? eState : 0);
}
static uint8 StMac_Ly4_Reduce(StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TEly4State eState = StMac_Ly4_State_Reduce;

    if (TRUE == pstFsmWorkData->stInEvents.bLy4FrReduceToCharging) {
        eState                                      = StMac_Ly4_State_Charging;
        pstFsmWorkData->stOutEvents.bEnterLy4Charging = TRUE;
    }

    if (StMac_Ly4_State_Reduce != eState) {
        pstFsmWorkData->stOutEvents.bExitLy4Reduce = TRUE;
    }

    return (uint8)((StMac_Ly4_NrOfState > eState) ? eState : 0);
}

static void StMac_ClearInEvents(StMac_TSfsmWorkData * pstFsmWorkData)
{
    uint8 u8ByteIndex = 0;
    for (u8ByteIndex = 0; u8ByteIndex < sizeof(pstFsmWorkData->stInEvents); u8ByteIndex++) {
        ((boolean *)(&(pstFsmWorkData->stInEvents)))[u8ByteIndex] = FALSE;
    }
}
static void StMac_ClearOutEvents(StMac_TSfsmWorkData * pstFsmWorkData)
{
    uint8 u8ByteIndex = 0;
    for (u8ByteIndex = 0; u8ByteIndex < sizeof(pstFsmWorkData->stOutEvents); u8ByteIndex++) {
        ((boolean *)(&(pstFsmWorkData->stOutEvents)))[u8ByteIndex] = FALSE;
    }
}
static void StMac_RunStateMachine(StMac_TSfsmObj * pstFsm, StMac_TSfsmWorkData * pstFsmWorkData)
{
    StMac_TSfsmObj * pstSubFsm   = NULL;
    StMac_TPFstateFun pfStateFun = pstFsm->pstFsmRun[*(pstFsm->pu8State)].pfStateFun;

    if (NULL != pfStateFun) {
        *(pstFsm->pu8State) = pfStateFun(pstFsmWorkData);
        pstSubFsm           = (StMac_TSfsmObj *)(pstFsm->pstFsmRun[*(pstFsm->pu8State)].pstSubFsm);
        if (NULL != pstSubFsm) {
            if (TRUE == *(pstSubFsm->pbEntryEvent)) {
                *(pstSubFsm->pu8State)          = pstSubFsm->u8DefaultState;
                *(pstSubFsm->pbDefaultOutEvent) = TRUE;
            }
            StMac_RunStateMachine(pstSubFsm, pstFsmWorkData);
        }
    } else {
        *(pstFsm->pu8State) = 0;
    }
}

void StMac_Init(StMac_TSfsmWorkData * pstFsmWorkData)
{
    const static StMac_TSfsmRun astLy1FsmRun[StMac_Ly1_NrOfState] = {
        {StMac_Ly1_Off,          NULL}, 
        {StMac_Ly1_Work,         (void *)(&(StMac_astFsms[StMac_Ly2_Fsm]))}, 
        {StMac_Ly1_Shutdown,     NULL}
    };

    const static StMac_TSfsmRun astLy2FsmRun[StMac_Ly2_NrOfState] = {
        {StMac_Ly2_Init,         NULL}, 
        {StMac_Ly2_Normal,       (void *)(&(StMac_astFsms[StMac_Ly3_Fsm]))}, 
        {StMac_Ly2_Fault,        NULL}
    };

    const static StMac_TSfsmRun astLy3FsmRun[StMac_Ly3_NrOfState] = {
        {StMac_Ly3_Standby,      NULL}, 
        {StMac_Ly3_Heating,      NULL}, 
        {StMac_Ly3_Charge,       (void *)(&(StMac_astFsms[StMac_Ly4_Fsm]))}, 
        {StMac_Ly3_Discharge,    NULL}
    };

    const static StMac_TSfsmRun astLy4FsmRun[StMac_Ly4_NrOfState] = {
        {StMac_Ly4_Charging,     NULL}, 
        {StMac_Ly4_Reduce,       NULL}
    };

    if (NULL == pstFsmWorkData) {
        return;
    }

    StMac_ClearInEvents(pstFsmWorkData);
    StMac_ClearOutEvents(pstFsmWorkData);
    pstFsmWorkData->au8State[StMac_Ly1_Fsm] = StMac_Ly1_State_Off;
    pstFsmWorkData->au8State[StMac_Ly2_Fsm] = StMac_Ly2_State_Init;
    pstFsmWorkData->au8State[StMac_Ly3_Fsm] = StMac_Ly3_State_Standby;
    pstFsmWorkData->au8State[StMac_Ly4_Fsm] = StMac_Ly4_State_Charging;

    StMac_astFsms[StMac_Ly1_Fsm].pstFsmRun         = astLy1FsmRun;
    StMac_astFsms[StMac_Ly1_Fsm].pu8State          = &(pstFsmWorkData->au8State[StMac_Ly1_Fsm]);
    StMac_astFsms[StMac_Ly1_Fsm].pbEntryEvent      = &(pstFsmWorkData->stOutEvents.bEnterLy1Off);
    StMac_astFsms[StMac_Ly1_Fsm].pbDefaultOutEvent = &(pstFsmWorkData->stOutEvents.bEnterLy1Off);
    StMac_astFsms[StMac_Ly1_Fsm].u8DefaultState    = pstFsmWorkData->au8State[StMac_Ly1_Fsm];

    StMac_astFsms[StMac_Ly2_Fsm].pstFsmRun         = astLy2FsmRun;
    StMac_astFsms[StMac_Ly2_Fsm].pu8State          = &(pstFsmWorkData->au8State[StMac_Ly2_Fsm]);
    StMac_astFsms[StMac_Ly2_Fsm].pbEntryEvent      = &(pstFsmWorkData->stOutEvents.bEnterLy1Work);
    StMac_astFsms[StMac_Ly2_Fsm].pbDefaultOutEvent = &(pstFsmWorkData->stOutEvents.bEnterLy2Init);
    StMac_astFsms[StMac_Ly2_Fsm].u8DefaultState    = pstFsmWorkData->au8State[StMac_Ly2_Fsm];

    StMac_astFsms[StMac_Ly3_Fsm].pstFsmRun         = astLy3FsmRun;
    StMac_astFsms[StMac_Ly3_Fsm].pu8State          = &(pstFsmWorkData->au8State[StMac_Ly3_Fsm]);
    StMac_astFsms[StMac_Ly3_Fsm].pbEntryEvent      = &(pstFsmWorkData->stOutEvents.bEnterLy2Normal);
    StMac_astFsms[StMac_Ly3_Fsm].pbDefaultOutEvent = &(pstFsmWorkData->stOutEvents.bEnterLy3Standby);
    StMac_astFsms[StMac_Ly3_Fsm].u8DefaultState    = pstFsmWorkData->au8State[StMac_Ly3_Fsm];

    StMac_astFsms[StMac_Ly4_Fsm].pstFsmRun         = astLy4FsmRun;
    StMac_astFsms[StMac_Ly4_Fsm].pu8State          = &(pstFsmWorkData->au8State[StMac_Ly4_Fsm]);
    StMac_astFsms[StMac_Ly4_Fsm].pbEntryEvent      = &(pstFsmWorkData->stOutEvents.bEnterLy3Charge);
    StMac_astFsms[StMac_Ly4_Fsm].pbDefaultOutEvent = &(pstFsmWorkData->stOutEvents.bEnterLy4Charging);
    StMac_astFsms[StMac_Ly4_Fsm].u8DefaultState    = pstFsmWorkData->au8State[StMac_Ly4_Fsm];
}

void StMac_RunCycle(StMac_TSfsmWorkData * pstFsmWorkData)
{
    if (NULL != pstFsmWorkData) {
        StMac_ClearOutEvents(pstFsmWorkData);
        StMac_RunStateMachine(StMac_astFsms, pstFsmWorkData);
        StMac_ClearInEvents(pstFsmWorkData);
    }
}
