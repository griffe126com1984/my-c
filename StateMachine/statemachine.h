#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "type.h"


typedef enum {
    StMac_Ly1_State_Off=0,
    StMac_Ly1_State_Work,
    StMac_Ly1_State_Shutdown,
    StMac_Ly1_NrOfState
} StMac_TEly1State;

typedef enum {
    StMac_Ly2_State_Init=0,
    StMac_Ly2_State_Normal,
    StMac_Ly2_State_Fault,
    StMac_Ly2_NrOfState
} StMac_TEly2State;

typedef enum {
    StMac_Ly3_State_Standby=0,
    StMac_Ly3_State_Heating,
    StMac_Ly3_State_Charge,
    StMac_Ly3_State_Discharge,
    StMac_Ly3_NrOfState
} StMac_TEly31State;


typedef enum {
    StMac_Ly4_State_Charging=0,
    StMac_Ly4_State_Reduce,
    StMac_Ly4_NrOfState
} StMac_TEly4State;

typedef enum {
    StMac_Ly1_Fsm=0,
    StMac_Ly2_Fsm,
    StMac_Ly3_Fsm,
    StMac_Ly4_Fsm,
    StMac_NrOfFsm
} StMac_TEfsms;

typedef struct {
    boolean bLy1FrOffToWork;
    boolean bLy1FrWorkToShutdown;
    boolean bLy1FrShutdownToWork;
    boolean bLy2FrInitToNormal;
    boolean bLy2FrInitToFault;
    boolean bLy2FrNormalToFault;
    boolean bLy2FrFaultToNormal;
    boolean bLy3FrStandbyToCharge;
    boolean bLy3FrStandbyToHeating;
    boolean bLy3FrStandbyToDisCharge;
    boolean bLy3FrChargeToStandby;
    boolean bLy3FrHeatingToStandby;
    boolean bLy3FrDisChargeToStandby;
    boolean bLy4FrChargingToReduce;
    boolean bLy4FrReduceToCharging;
} StMac_TSinEvents;

typedef struct {
    boolean bEnterLy1Off;
    boolean bEnterLy1Work;
    boolean bEnterLy1Shutdown;
    boolean bEnterLy2Init;
    boolean bEnterLy2Normal;
    boolean bEnterLy2Fault;
    boolean bEnterLy3Standby;
    boolean bEnterLy3Heating;
    boolean bEnterLy3Charge;
    boolean bEnterLy3Discharge;
    boolean bEnterLy4Charging;
    boolean bEnterLy4Reduce;
    boolean bExitLy1Off;
    boolean bExitLy1Work;
    boolean bExitLy1Shutdown;
    boolean bExitLy2Init;
    boolean bExitLy2Normal;
    boolean bExitLy2Fault;
    boolean bExitLy3Standby;
    boolean bExitLy3Heating;
    boolean bExitLy3Charge;
    boolean bExitLy3Reduce;
    boolean bExitLy3Discharge;
    boolean bExitLy4Charging;
    boolean bExitLy4Reduce;
} StMac_TSoutEvents;

typedef struct {
    StMac_TSinEvents stInEvents;
    StMac_TSoutEvents stOutEvents;
    uint8 au8State[StMac_NrOfFsm];
} StMac_TSfsmWorkData;

typedef uint8 (* StMac_TPFstateFun)(StMac_TSfsmWorkData* pstFsmWorkData);

typedef struct {
    StMac_TPFstateFun pfStateFun;
    void* pstSubFsm;
} StMac_TSfsmRun;

    
typedef struct {
    const StMac_TSfsmRun* pstFsmRun;
    uint8* pu8State;
    boolean* pbEntryEvent;
    boolean* pbDefaultOutEvent;
    uint8 u8DefaultState;
} StMac_TSfsmObj;

void StMac_Init(StMac_TSfsmWorkData* pstFsmWorkData);
void StMac_RunCycle(StMac_TSfsmWorkData* pstFsmWorkData);

#endif

