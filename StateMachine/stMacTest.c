#include "stdio.h"
#include "stdlib.h"
#include "statemachine.h"

static StMac_TSfsmWorkData StMac_stFsmWorkData;

static void OutStateInfo(void)
{
    char ly1State[StMac_Ly1_NrOfState][100] = {
        "StMac_Ly1_State_Off",
        "StMac_Ly1_State_Work",
        "StMac_Ly1_State_Shutdown",
    };
    char ly2State[StMac_Ly2_NrOfState][100] = {
        "StMac_Ly2_State_Init",
        "StMac_Ly2_State_Normal",
        "StMac_Ly2_State_Fault",
    };
    char ly3State[StMac_Ly3_NrOfState][100] = {
        "StMac_Ly3_State_Standby",
        "StMac_Ly3_State_Heating",
        "StMac_Ly3_State_Charge",
        "StMac_Ly3_State_Discharge",
    };
    char ly4State[StMac_Ly4_NrOfState][100] = {
        "StMac_Ly4_State_Charging",
        "StMac_Ly4_State_Reduce",
    };

    printf("The state of Ly1 state machine is %s\n", ly1State[StMac_stFsmWorkData.au8State[StMac_Ly1_Fsm]]);
    printf("The state of Ly1 state machine is %s\n", ly2State[StMac_stFsmWorkData.au8State[StMac_Ly2_Fsm]]);
    printf("The state of Ly1 state machine is %s\n", ly3State[StMac_stFsmWorkData.au8State[StMac_Ly3_Fsm]]);
    printf("The state of Ly1 state machine is %s\n", ly4State[StMac_stFsmWorkData.au8State[StMac_Ly4_Fsm]]);
}


static void OutEventInfo(void)
{
    char event[25][100] = {
        "bEnterLy1Off",
        "bEnterLy1Work",
        "bEnterLy1Shutdown",
        "bEnterLy2Init",
        "bEnterLy2Normal",
        "bEnterLy2Fault",
        "bEnterLy3Standby",
        "bEnterLy3Heating",
        "bEnterLy3Charge",
        "bEnterLy3Discharge",
        "bEnterLy4Charging",
        "bEnterLy4Reduce",
        "bExitLy1Off",
        "bExitLy1Work",
        "bExitLy1Shutdown",
        "bExitLy2Init",
        "bExitLy2Normal",
        "bExitLy2Fault",
        "bExitLy3Standby",
        "bExitLy3Heating",
        "bExitLy3Charge",
        "bExitLy3Reduce",
        "bExitLy3Discharge",
        "bExitLy4Charging",
        "bExitLy4Reduce",
    };
    printf("------------------------------------------------------------\n");
    for(uint8 i=0; i < sizeof(StMac_stFsmWorkData.stOutEvents); i++) {
        if(TRUE == ((boolean*)(&(StMac_stFsmWorkData.stOutEvents)))[i]) {
            printf("Out event: %s is true\n", event[i]);
        }
    }
    printf("------------------------------------------------------------\n");
}

int main(void)
{
    char op=0;
    StMac_Init(&StMac_stFsmWorkData);
    do {
        system("cls");
        OutStateInfo();
        printf("Please trigger input event:\n");
        printf("============================================================\n");
        printf("[A]: bLy1FrOffToWork\n");
        printf("[B]: bLy1FrShutdownToWork\n");
        printf("[C]: bLy1FrWorkToShutdown\n");
        printf("[D]: bLy2FrInitToFault\n");
        printf("[E]: bLy2FrInitToNormal\n");
        printf("[F]: bLy2FrNormalToFault\n");
        printf("[G]: bLy2FrFaultToNormal\n");
        printf("[H]: bLy3FrStandbyToCharge\n");
        printf("[I]: bLy3FrStandbyToHeating\n");
        printf("[J]: bLy3FrStandbyToDisCharge\n");
        printf("[K]: bLy3FrChargeToStandby\n");
        printf("[L]: bLy3FrHeatingToStandby\n");
        printf("[M]: bLy3FrDisChargeToStandby\n");
        printf("[N]: bLy4FrChargingToReduce\n");
        printf("[O]: bLy4FrReduceToCharging\n");
        printf("============================================================\n");
        fflush(stdin);
        printf("Your choice is: ");
        scanf("%c",&op);
        printf("------------------------------------------------------------\n");
        
        switch (op) {
            case 'a':
            case 'A':
                StMac_stFsmWorkData.stInEvents.bLy1FrOffToWork = TRUE;
                break;
            case 'b':
            case 'B':
                StMac_stFsmWorkData.stInEvents.bLy1FrShutdownToWork = TRUE;
                break;
            case 'c':
            case 'C':
                StMac_stFsmWorkData.stInEvents.bLy1FrWorkToShutdown = TRUE;
                break;
            case 'd':
            case 'D':
                StMac_stFsmWorkData.stInEvents.bLy2FrInitToFault = TRUE;
                break;
            case 'e':
            case 'E':
                StMac_stFsmWorkData.stInEvents.bLy2FrInitToNormal = TRUE;
                break;
            case 'f':
            case 'F':
                StMac_stFsmWorkData.stInEvents.bLy2FrNormalToFault = TRUE;
                break;
            case 'g':
            case 'G':
                StMac_stFsmWorkData.stInEvents.bLy2FrFaultToNormal = TRUE;
                break;
            case 'h':
            case 'H':
                StMac_stFsmWorkData.stInEvents.bLy3FrStandbyToCharge = TRUE;
                break;
            case 'i':
            case 'I':
                StMac_stFsmWorkData.stInEvents.bLy3FrStandbyToHeating = TRUE;
                break;
            case 'j':
            case 'J':
                StMac_stFsmWorkData.stInEvents.bLy3FrStandbyToDisCharge = TRUE;
                break;
            case 'k':
            case 'K':
                StMac_stFsmWorkData.stInEvents.bLy3FrChargeToStandby = TRUE;
                break;
            case 'l':
            case 'L':
                StMac_stFsmWorkData.stInEvents.bLy3FrHeatingToStandby = TRUE;
                break;
            case 'm':
            case 'M':
                StMac_stFsmWorkData.stInEvents.bLy3FrDisChargeToStandby = TRUE;
                break;
            case 'n':
            case 'N':
                StMac_stFsmWorkData.stInEvents.bLy4FrChargingToReduce = TRUE;
                break;
            case 'o':
            case 'O':
                StMac_stFsmWorkData.stInEvents.bLy4FrReduceToCharging = TRUE;
                break;
            default:
                printf("wrong input\n");
                break;
        }
        StMac_RunCycle(&StMac_stFsmWorkData);
        OutStateInfo();
        OutEventInfo();
        system("pause");
    } while(1);
    return 0;
}
