#include "stdio.h"
#include "stdlib.h"
#include "sort.h"

uint8 TestArray[] = {100,100,99,98,97,97,96,95,95,94,93,92,91,90,90,90};

void MI_PrintArray(uint8 * pu8Array, uint16 u16ArrSize)
{
    uint16 u16Index;
    for (u16Index = 0; u16Index < u16ArrSize; u16Index++) {
        printf("%d,",pu8Array[u16Index]);
    }
    printf("\n------------------------------------------------------------\n");
}

int main(void)
{
    uint8 op;
    uint16 u16Index    = 0;
    uint8 * pu8TempArr = (uint8 *)malloc(sizeof(TestArray));

    TSsortClass sortClass;

    do {
        for (u16Index = 0; u16Index < sizeof(TestArray); u16Index++) {
            pu8TempArr[u16Index] = TestArray[u16Index];
        }
        system("cls");
        printf("Please choose action:\n");
        printf("============================================================\n");
        printf("[A]: Bubble sort\n");
        printf("[B]: Select sort\n");
        printf("[C]: Insert sort\n");
        printf("[D]: Fast sort\n");
        printf("============================================================\n");
        fflush(stdin);
        printf("Your choice is: ");
        scanf("%c", &op);
        switch (op) {
            case 'a':
            case 'A':
                sortClass.pfSortFunc = SRT_BubbleSort;
                printf("Use bubble to sort the array\n");
                printf("------------------------------------------------------------\n");
                break;
            case 'b':
            case 'B':
                sortClass.pfSortFunc = SRT_SelectSort;
                printf("Use select to sort the array\n");
                printf("------------------------------------------------------------\n");
                break;
            case 'c':
            case 'C':
                sortClass.pfSortFunc = SRT_InsertSort;
                printf("Use insert to sort the array\n");
                printf("------------------------------------------------------------\n");
                break;
            case 'd':
            case 'D':
                sortClass.pfSortFunc = SRT_FastSort;
                printf("Use fast to sort the array\n");
                printf("------------------------------------------------------------\n");
                break;
            default:
                printf("************************************************************\n");
                printf("Invalid choice, nothing to do\n");
                printf("************************************************************\n");
                break;
        }

        /* MI_PrintArray(pu8TempArr, sizeof(TestArray)); */

        sortClass.pu8Array   = pu8TempArr;
        sortClass.u16ArrSize = sizeof(TestArray);

        sortClass.pfSortFunc(sortClass.pu8Array, sortClass.u16ArrSize);

        printf("The result is:----------------------------------------------\n");
        printf("------------------------------------------------------------\n");
        MI_PrintArray(pu8TempArr, sizeof(TestArray));

        system("pause");
    } while (1);

    return 0;
}
