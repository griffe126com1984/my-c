#ifndef INFOCODE_H
#define INFOCODE_H

#include "type.h"

#define MAX_INFO_CODE        ((uint8)100)

extern uint16 SetInfoCode(uint8 code);
extern uint16 ClearInfoCode(uint8 code);

#endif
