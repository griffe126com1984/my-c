#include "infocode.h"
#include "stdlib.h"

#define LINKLIST_METHOD

#define TRUE ((boolean)1)
#define FALSE ((boolean)0)

#define CHECK_CODE_STS(code) ((0 != ((au8CodeMask[((code) >> 3)]) & ((uint8)(1 << ((uint8)((code)&0x07)))))) ? TRUE : FALSE)
#define SET_CODE_STS(code)   ((au8CodeMask[((code) >> 3)]) |= ((uint8)(1 << ((uint8)((code)&0x07)))))
#define CLEAR_CODE_STS(code) ((au8CodeMask[((code) >> 3)]) &= ((uint8)(~((uint8)(1 << ((uint8)((code)&0x07)))))))

static uint8 au8CodeMask[((uint8)(MAX_INFO_CODE>>3)) + 1] = {0};

#ifndef LINKLIST_METHOD
static uint8 u8LatestInfoCode = 0;
static uint8 au8CodeList[MAX_INFO_CODE+1] = {0};

uint16 SetInfoCode(uint8 code)
{
    if (MAX_INFO_CODE >= code) {
        if (TRUE != CHECK_CODE_STS(code)) {
            SET_CODE_STS(code);
            au8CodeList[code] = u8LatestInfoCode;
            u8LatestInfoCode  = code;
        } else {
        }
    }
    return u8LatestInfoCode;
}

uint16 ClearInfoCode(uint8 code)
{
    if (MAX_INFO_CODE >= code) {
        if (TRUE == CHECK_CODE_STS(code)) {
            CLEAR_CODE_STS(code);
            if (u8LatestInfoCode == code) {
                do {
                    u8LatestInfoCode = au8CodeList[u8LatestInfoCode];
                    if (TRUE == CHECK_CODE_STS(u8LatestInfoCode)) {
                        break;
                    } else {
                        continue;
                    }
                } while (0 != u8LatestInfoCode);
            }
        } else {
        }
    }
    return u8LatestInfoCode;
}
#else

#include "linklist.h"
static TPSnode psCodeList = NULL;

uint16 SetInfoCode(uint8 code)
{
    if (NULL == psCodeList) {
        psCodeList = InitCodeList();            /* Initialize the code list */
    }

    if (MAX_INFO_CODE >= code) {
        if (TRUE != CHECK_CODE_STS(code)) {     /* Code didn't be set before */
            SET_CODE_STS(code);                 /* Set the bit mask for this code */
            InsertCodeNode(psCodeList,code);    /* Insert the code node afther the head */
        } else {
        }
    }

    return psCodeList->pstNext->u16Code;        /* Return the latest code */
}

uint16 ClearInfoCode(uint8 code)
{
    if (MAX_INFO_CODE >= code) {
        if (TRUE == CHECK_CODE_STS(code)) {     /* Code has been set before */
            CLEAR_CODE_STS(code);               /* Clear the bit mask of this code */
            DeleteCodeNode(psCodeList,code);    /* Delete the code node in the list */
        } else {
        }
    }

    return psCodeList->pstNext->u16Code;        /* Return the latest code */
}

#endif
