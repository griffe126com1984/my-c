#ifndef LINKLIST_H
#define LINKLIST_H

#include "type.h"

typedef struct STlistNode {
    uint16 u16Code;
    struct STlistNode* pstNext;
} TSnode, * TPSnode;

TPSnode InitCodeList();
void InsertCodeNode(TPSnode psList, uint16 u16Code);
void DeleteCodeNode(TPSnode psList, uint16 u16Code);

#endif