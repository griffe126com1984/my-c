#include "linklist.h"
#include "stdlib.h"


TPSnode InitCodeList()
{
    TPSnode psHead;

    psHead = (TPSnode)malloc(sizeof(TSnode));    /* Apply RAM for list head */
    psHead->pstNext = NULL;                      /* Point to the end of this list */
    
    InsertCodeNode(psHead, 0);                   /* Insert code "0" as the last element of this list */

    return psHead;                               /* Return the address of the head as the address of this list */
}

void InsertCodeNode(TPSnode psList, uint16 u16Code)
{
    TPSnode psCodeNode;                                  /* declare the pointer point to the new node */

    if (NULL != psList) {         
        psCodeNode = (TPSnode)malloc(sizeof(TSnode));    /* Apply RAM for the new node */
        psCodeNode->pstNext = psList->pstNext;           /* Insert the new node between the head and the node behind the head */
        psCodeNode->u16Code = u16Code;                   /* Set the value of the new node */
        psList->pstNext     = psCodeNode;                /* Insert the new node between the head and the node behind the head */
    }
}

void DeleteCodeNode(TPSnode psList, uint16 u16Code)
{
    TPSnode psPreCodeNode;                                          /* Declare the pointer point to the node before the one which will be deleted */
    TPSnode psDelCodeNode;                                          /* Declare the pointer point to the node which will be deleted */

    if (NULL != psList) {
        psPreCodeNode = psList;                                     /* Init the pre node to the head of this list */
        psDelCodeNode = psPreCodeNode->pstNext;                     /* Init the delete node the first node */
        while (psDelCodeNode != NULL) {                             /* Stop loop when reached the end of this list */
            if (u16Code == psDelCodeNode->u16Code) {                /* Find the node which will be deleted */
                psPreCodeNode->pstNext = psDelCodeNode->pstNext;    /* Link the node behind the deleted node to the pre node */
                free(psDelCodeNode);                                /* Free RAM for the deleted node */
                return;
            } else {                                                /* Not find the node which will be deleted */
                psPreCodeNode = psDelCodeNode;                      /* Point to the next one */
                psDelCodeNode = psDelCodeNode->pstNext;             /* Point to the next one */
            }
        }
    }
}