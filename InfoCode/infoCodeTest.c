#include "stdio.h"
#include "stdlib.h"
#include "infocode.h"


int main(void)
{
    char op=0;
    uint16 code=0;
    do {
        system("cls");
        printf("Please choose action:\n");
        printf("============================================================\n");
        printf("[A]: Set info code\n");
        printf("[B]: Clear info code\n");
        printf("============================================================\n");
        fflush(stdin);
        printf("Your choice is: ");
        scanf("%c",&op);
        printf("------------------------------------------------------------\n");
        
        switch (op) {
            case 'a':
            case 'A':
                printf("Entern the code[1-%d] you want to set: ", MAX_INFO_CODE);
                fflush(stdin);
                scanf("%d", &code);
                printf("************************************************************\n");
                if ((1 <= code) && (MAX_INFO_CODE >= code)) {
                    printf("The latest code is: %d\n", SetInfoCode((uint8)code));
                } else {
                    printf("The value is out of the range\n");
                }
                printf("************************************************************\n");
                break;
            case 'b':
            case 'B':
                printf("Entern the code[1-%d] you want to clear: ", MAX_INFO_CODE);
                fflush(stdin);
                scanf("%d", &code);
                printf("************************************************************\n");
                if ((1 <= code) && (MAX_INFO_CODE >= code)) {
                    printf("The latest code is: %d\n", ClearInfoCode((uint8)code));
                } else {
                    printf("The value is out of the range\n");
                }
                printf("************************************************************\n");
                break;
            default:
                printf("************************************************************\n");
                printf("Invalid choice, nothing to do\n");
                printf("************************************************************\n");
                break;
        }
        system("pause");
    } while(1);
    return 0;
}
