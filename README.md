# my-c
---

## Introduction
For learning C language.

## Content
+ **C, VSCode, gcc**
+ **StateMachine ->** A software implementation template for HFSM. Make it via recursive calling and function pointer.
+ **InfoCode ->** Refreshing information code via two method(Linked List/Array).
    - Always return the latest active information code.
    - When the latest information code is inactive, return the active one just earlier the latest one.
+ **CrcCaculator ->** The implementation for several CRC algorithms follow AutoSAR standard.
    - Configurable.
    - CrcCaculator.exe is the small testing tool.

