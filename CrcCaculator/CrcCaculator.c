#include "stdlib.h"
#include "stdio.h"
#include "CrcConfig.h"
#include "CrcCore.h"

int main(void)
{
    uint8 i = 0;
    uint8 u8DataLen = 0;
    uint8 *au8Data;
    uint16 u16CrcCode;
    uint16 u16DataId;
    uint8 au8Array[2];

    do {
        printf("***************************************************************************\n");
        printf("My dear colleague :-)\n");
        printf("Please enter the length of data you want to caculate \n");
        printf("Data length: ");
        scanf("%d", (int *)&u8DataLen);
        printf("***************************************************************************\n");
        printf("Please choose data ID for final check \n");
        printf("[1] DSP_TX: 0x91DC \n");
        printf("[2] DSP_RX: 0xB794 \n");
        printf("[3] No final check \n");
        printf("***************************************************************************\n");
        printf("Your choice: ");
        scanf("%d", (int *)&u16DataId);
        if (1 == u16DataId) {
            u16DataId = 0x91dc;
        } else if (2 == u16DataId) {
            u16DataId = 0xb794;
        } else {
            u16DataId = 0;
        }
        printf("***************************************************************************\n");
        printf("Now, please enter the data one bye one\n");
        printf("***************************************************************************\n");
        au8Data = (uint8 *)calloc(u8DataLen, sizeof(uint8));
        for (i = 0; i < u8DataLen; i++) {
            if (10 > i) {
                printf("Data[%d]  = 0x", i);
            } else {
                printf("Data[%d] = 0x", i);
            }
            scanf("%x", (au8Data + i));
        }
        Crc_CaculateCRC16FL(0xFFFF, (uint8 *)au8Data, u8DataLen, &u16CrcCode, TRUE);
        if (0 != u16DataId) {
            au8Array[0] = (uint8)(u16DataId & 0x0ff);
            au8Array[1] = (uint8)(u16DataId >> 8);
            Crc_CaculateCRC16FL(u16CrcCode, au8Array, 2, &u16CrcCode, FALSE);
        }
        printf("***************************************************************************\n");
        printf("The CRC code is 0x%X\n", u16CrcCode);
        printf("***************************************************************************\n");
        free(au8Data);
        system("pause");
        system("cls");
    } while (1);
    return 0;
}