/*******************************************************************************
*
* @file   CrcCore.c
* @date   16.06.2017
* @author zhang135
*
******************************************************************************/

#include "CrcCore.h"

/* PRQA S 3218,3450,3212,3120,3132,0492,0499,0488,0489,3673,3393 ++ */

#if (defined(CRC_IN_REFLECT)||defined(CRC_OUT_REFLECT))
static uint8 Crc_RefelctByte(uint8 data);
static void Crc_RefelctArray(uint8 *data, uint8 len);

static uint8 Crc_RefelctByte(uint8 data)
{
	uint8 temData=0;
	uint8 i=0;
	for(i=0;i<BYTE_WIDTH;i++) {
		if(0!=(data&(uint8)(1u<<i))) {
			temData |= (uint8)(1u<<(BYTE_WIDTH-1-i));
		}
	}
	return temData;
}

static void Crc_RefelctArray(uint8 *data, uint8 len) {
	uint8 i = 0;
	uint8 temData = 0;
	uint8 halfLen = 0;
	if(0!=(len%2)) {
		halfLen = (len/2)+1;
	} else {
		halfLen = len/2;
	}
	for(i=0;i<halfLen;i++) {
		temData = data[i];
		data[i] = Crc_RefelctByte(data[len-1-i]);
		data[len-1-i] = Crc_RefelctByte(temData);
	}

}
#endif

#if (STD_ON == CRC8SAE_ASR)

#	if ((STD_OFF == CRC8SAE_TABLE_USED)||(defined(CRC8SAE_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC8SAE_ViaBit(CRC_T8 init, uint8 data, CRC_T8 *crcResult)
{
	uint8 i;
	*crcResult = init;
#		if (CRC8SAE_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC8SAE_WIDTH);
	*crcResult ^= (CRC_T8)data;
#		else
	*crcResult ^= ((CRC_T8)data<<(CRC8SAE_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#		endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC8_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC8SAE_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#		if (CRC8SAE_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC8SAE_WIDTH);
#		endif
}
#	endif

#	if (STD_ON == CRC8SAE_TABLE_USED)
#		ifdef CRC8SAE_TABLE_TOBEDONE
static CRC_T8 CRC8SAE_Table[CRC_TABLE_LEN];
#		else
	const CRC_T8 CRC8SAE_Table[CRC_TABLE_LEN] = CRC8SAE_TABLE;
#		endif
static void Crc_CaculateByteCRC8SAE_ViaTable(CRC_T8 init, uint8 data, CRC_T8 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC8SAE_TABLE_TOBEDONE
	uint16 i;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC8SAE_ViaBit(0,(uint8)i,(CRC8SAE_Table+i));
		}
		firstCall = FALSE;
	}
#		endif
	*crcResult = init;
#		if(CRC8SAE_WIDTH>=BYTE_WIDTH)
	crcH = *crcResult>>(CRC8SAE_WIDTH - BYTE_WIDTH);
#		else
	crcH = *crcResult<<(BYTE_WIDTH - CRC8SAE_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC8SAE_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC8SAE(CRC_T8 init, uint8 *data, uint32 len, CRC_T8 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC8SAE_XORVALUE;
#	if (STD_ON == CRC8SAE_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T8));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC8SAE_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC8SAE_TABLE_USED)
		Crc_CaculateByteCRC8SAE_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC8SAE_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC8SAE_XORVALUE;

#	if (STD_ON == CRC8SAE_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T8));
#	endif
}

#	if (STD_ON == CRC8SAE_CHECK_ENABLE)
void Crc_CaculateCRC8SAE_Check(CRC_T8 init, CRC_T8 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC8SAE(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC8SAE_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC8SAE_MagicCheck(CRC_T8 init,  uint8 *data, uint32 len, CRC_T8 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T8);
	uint8 newArr[sizeof(CRC_T8)];
	Crc_CaculateCRC8SAE(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {	/* PRQA S 2465 */
#		if (STD_ON == CRC8SAE_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC8SAE(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC8SAE_XORVALUE;
}
#	endif

#endif

#if (STD_ON == CRC8H2F_ASR)

#	if ((STD_OFF == CRC8H2F_TABLE_USED)||(defined(CRC8H2F_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC8H2F_ViaBit(CRC_T8 init, uint8 data, CRC_T8 *crcResult)
{
	uint8 i;
	*crcResult = init;
#if (CRC8H2F_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC8H2F_WIDTH);
	*crcResult ^= (CRC_T8)data;
#else
	*crcResult ^= ((CRC_T8)data<<(CRC8H2F_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC8_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC8H2F_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#if (CRC8H2F_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC8H2F_WIDTH);
#endif
}
#	endif

#	if (STD_ON == CRC8H2F_TABLE_USED)
#		ifdef CRC8H2F_TABLE_TOBEDONE
static CRC_T8 CRC8H2F_Table[CRC_TABLE_LEN];
#		else
const static CRC_T8 CRC8H2F_Table[CRC_TABLE_LEN] = CRC8H2F_TABLE;
#		endif
static void Crc_CaculateByteCRC8H2F_ViaTable(CRC_T8 init, uint8 data, CRC_T8 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC8H2F_TABLE_TOBEDONE
	uint16 i=0;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC8H2F_ViaBit(0,i,(CRC8H2F_Table+i));
		}
		firstCall = FALSE;
	}
#		endif
	*crcResult = init;
#		if(CRC8H2F_WIDTH>=BYTE_WIDTH)
	crcH = *crcResult>>(CRC8H2F_WIDTH - BYTE_WIDTH);
#		else
	crcH = *crcResult<<(BYTE_WIDTH - CRC8H2F_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC8H2F_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC8H2F(CRC_T8 init, uint8 *data, uint32 len, CRC_T8 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC8H2F_XORVALUE;
#	if (STD_ON == CRC8H2F_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T8));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC8H2F_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC8H2F_TABLE_USED)
		Crc_CaculateByteCRC8H2F_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC8H2F_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC8H2F_XORVALUE;

#	if (STD_ON == CRC8H2F_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T8));
#	endif
}

#	if (STD_ON == CRC8H2F_CHECK_ENABLE)
void Crc_CaculateCRC8H2F_Check(CRC_T8 init, CRC_T8 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC8H2F(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC8H2F_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC8H2F_MagicCheck(CRC_T8 init,  uint8 *data, uint32 len, CRC_T8 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T8);
	uint8 newArr[sizeof(CRC_T8)];
	Crc_CaculateCRC8H2F(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {  /* PRQA S 2465 */
#		if (STD_ON == CRC8H2F_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC8H2F(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC8H2F_XORVALUE;
}
#	endif

#endif

#if (STD_ON == CRC16FL_ASR)

#	if ((STD_OFF == CRC16FL_TABLE_USED)||(defined(CRC16FL_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC16FL_ViaBit(CRC_T16 init, uint8 data, CRC_T16 *crcResult);
static void Crc_CaculateByteCRC16FL_ViaBit(CRC_T16 init, uint8 data, CRC_T16 *crcResult)
{
	uint8 i;
	*crcResult = init;
#if (CRC16FL_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC16FL_WIDTH);
	*crcResult ^= (CRC_T16)data;
#else
	*crcResult ^= ((CRC_T16)data<<(CRC16FL_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC16_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC16FL_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#if (CRC16FL_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC16FL_WIDTH);
#endif
}
#	endif

#	if (STD_ON == CRC16FL_TABLE_USED)
#		ifdef CRC16FL_TABLE_TOBEDONE
static CRC_T16 CRC16FL_Table[CRC_TABLE_LEN];
#		else
const static CRC_T16 CRC16FL_Table[CRC_TABLE_LEN] = CRC16FL_TABLE;
#		endif
static void Crc_CaculateByteCRC16FL_ViaTable(CRC_T16 init, uint8 data, CRC_T16 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC16FL_TABLE_TOBEDONE
	uint16 i=0;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC16FL_ViaBit(0,i,(CRC16FL_Table+i));
		}
		firstCall = FALSE;
	}
#		endif
	*crcResult = init;
#		if(CRC16FL_WIDTH>=BYTE_WIDTH)
		crcH = (uint8)(*crcResult>>(CRC16FL_WIDTH - BYTE_WIDTH));
#		else
		crcH = *crcResult<<(BYTE_WIDTH - CRC16FL_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC16FL_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC16FL(CRC_T16 init, uint8 *data, uint32 len, CRC_T16 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC16FL_XORVALUE;
/* #	if (STD_ON == CRC16FL_OUT_REFLECT) */
#	if (STD_ON == CRC16FL_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T16));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC16FL_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC16FL_TABLE_USED)
		Crc_CaculateByteCRC16FL_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC16FL_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC16FL_XORVALUE;

#	if (STD_ON == CRC16FL_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T16));
#	endif
}

#	if (STD_ON == CRC16FL_CHECK_ENABLE)
void Crc_CaculateCRC16FL_Check(CRC_T16 init, CRC_T16 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC16FL(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC16FL_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC16FL_MagicCheck(CRC_T16 init,  uint8 *data, uint32 len, CRC_T16 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T16);
	uint8 newArr[sizeof(CRC_T16)];
	Crc_CaculateCRC16FL(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {
#		if (STD_ON == CRC16FL_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC16FL(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC16FL_XORVALUE;
}
#	endif

#endif

#if (STD_ON == CRC32NA_ASR)

#	if ((STD_OFF == CRC32NA_TABLE_USED)||(defined(CRC32NA_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC32NA_ViaBit(CRC_T32 init, uint8 data, CRC_T32 *crcResult)
{
	uint8 i;
	*crcResult = init;
#if (CRC32NA_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC32NA_WIDTH);
	*crcResult ^= (CRC_T32)data;
#else
	*crcResult ^= ((CRC_T32)data<<(CRC32NA_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC32_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC32NA_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#if (CRC32NA_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC32NA_WIDTH);
#endif
}
#	endif

#	if (STD_ON == CRC32NA_TABLE_USED)
#	ifdef CRC32NA_TABLE_TOBEDONE
static CRC_T32 CRC32NA_Table[CRC_TABLE_LEN];
#	else
const static CRC_T32 CRC32NA_Table[CRC_TABLE_LEN] = CRC32NA_TABLE;
#	endif
static void Crc_CaculateByteCRC32NA_ViaTable(CRC_T32 init, uint8 data, CRC_T32 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC32NA_TABLE_TOBEDONE
	uint16 i=0;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC32NA_ViaBit(0,i,(CRC32NA_Table+i));
		}
		firstCall = FALSE;
	}
#	endif
	*crcResult = init;
#		if(CRC32NA_WIDTH>=BYTE_WIDTH)
	crcH = *crcResult>>(CRC32NA_WIDTH - BYTE_WIDTH);
#		else
	crcH = *crcResult<<(BYTE_WIDTH - CRC32NA_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC32NA_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC32NA(CRC_T32 init, uint8 *data, uint32 len, CRC_T32 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC32NA_XORVALUE;
#	if (STD_ON == CRC32NA_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T32));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC32NA_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC32NA_TABLE_USED)
		Crc_CaculateByteCRC32NA_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC32NA_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC32NA_XORVALUE;

#	if (STD_ON == CRC32NA_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T32));
#	endif
}

#	if (STD_ON == CRC32NA_CHECK_ENABLE)
void Crc_CaculateCRC32NA_Check(CRC_T32 init, CRC_T32 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC32NA(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC32NA_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC32NA_MagicCheck(CRC_T32 init,  uint8 *data, uint32 len, CRC_T32 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T32);
	uint8 newArr[sizeof(CRC_T32)];
	Crc_CaculateCRC32NA(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {
#		if (STD_ON == CRC32NA_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC32NA(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC32NA_XORVALUE;
}
#	endif

#endif

#if (STD_ON == CRC32P4_ASR)

#	if ((STD_OFF == CRC32P4_TABLE_USED)||(defined(CRC32P4_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC32P4_ViaBit(CRC_T32 init, uint8 data, CRC_T32 *crcResult)
{
	uint8 i;
	*crcResult = init;
#		if (CRC32P4_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC32P4_WIDTH);
	*crcResult ^= (CRC_T32)data;
#		else
	*crcResult ^= ((CRC_T32)data<<(CRC32P4_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#		endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC32_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC32P4_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#		if (CRC32P4_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC32P4_WIDTH);
#		endif
}
#	endif

#	if (STD_ON == CRC32P4_TABLE_USED)
#		ifdef CRC32P4_TABLE_TOBEDONE
static CRC_T32 CRC32P4_Table[CRC_TABLE_LEN];
#		else
const static CRC_T32 CRC32P4_Table[CRC_TABLE_LEN] = CRC32P4_TABLE;
#		endif
static void Crc_CaculateByteCRC32P4_ViaTable(CRC_T32 init, uint8 data, CRC_T32 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC32P4_TABLE_TOBEDONE
	uint16 i=0;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC32P4_ViaBit(0,(uint8)i,(CRC32P4_Table+i));
		}
		firstCall = FALSE;
	}
#		endif
	*crcResult = init;
#		if(CRC32P4_WIDTH>=BYTE_WIDTH)
	crcH = (uint8)(*crcResult>>(CRC32P4_WIDTH - BYTE_WIDTH));
#		else
	crcH = *crcResult<<(BYTE_WIDTH - CRC32P4_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC32P4_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC32P4(CRC_T32 init, uint8 *data, uint32 len, CRC_T32 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC32P4_XORVALUE;
#	if (STD_ON == CRC32P4_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T32));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC32P4_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC32P4_TABLE_USED)
		Crc_CaculateByteCRC32P4_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC32P4_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC32P4_XORVALUE;

#	if (STD_ON == CRC32P4_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T32));
#	endif
}

#	if (STD_ON == CRC32P4_CHECK_ENABLE)
void Crc_CaculateCRC32P4_Check(CRC_T32 init, CRC_T32 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC32P4(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC32P4_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC32P4_MagicCheck(CRC_T32 init,  uint8 *data, uint32 len, CRC_T32 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T32);
	uint8 newArr[sizeof(CRC_T32)];
	Crc_CaculateCRC32P4(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {
#		if (STD_ON == CRC32P4_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC32P4(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC32P4_XORVALUE;
}
#	endif

#endif

#if (STD_ON == CRC64NA_ASR)

#	if ((STD_OFF == CRC64NA_TABLE_USED)||(defined(CRC64NA_TABLE_TOBEDONE)))
static void Crc_CaculateByteCRC64NA_ViaBit(CRC_T64 init, uint8 data, CRC_T64 *crcResult)
{
	uint8 i;
	*crcResult = init;
#		if (CRC64NA_WIDTH<BYTE_WIDTH)
	*crcResult<<=(BYTE_WIDTH-CRC64NA_WIDTH);
	*crcResult ^= (CRC_T64)data;
#		else
	*crcResult ^= ((CRC_T64)data<<(CRC64NA_WIDTH-BYTE_WIDTH)); /* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-8)) */
#		endif
	for(i=0;i<BYTE_WIDTH;i++) {
		/* (Rn(x)+Bn-1(x)<<(CRC_WIDTH-BYTE_WIDTH))<<8)%G(x) */
		if(0!=(*crcResult&CRC64_MSB_MASK)) {
			*crcResult <<= 1;
			*crcResult ^= CRC64NA_POLY;
		} else {
			*crcResult <<= 1;
		}
	}
#		if (CRC64NA_WIDTH < BYTE_WIDTH)
	*crcResult>>=(BYTE_WIDTH-CRC64NA_WIDTH);
#		endif
}
#	endif

#	if (STD_ON == CRC64NA_TABLE_USED)
#		ifdef CRC64NA_TABLE_TOBEDONE
static CRC_T64 CRC64NA_Table[CRC_TABLE_LEN];
#		else
const static CRC_T64 CRC64NA_Table[CRC_TABLE_LEN] = CRC64NA_TABLE;
#		endif

static void Crc_CaculateByteCRC64NA_ViaTable(CRC_T64 init, uint8 data, CRC_T64 *crcResult)
{
	uint8 crcH=0;
#		ifdef CRC64NA_TABLE_TOBEDONE
	uint16 i=0;
	static uint8 firstCall = TRUE;
	if(TRUE == firstCall) {
		for(i=0;i<CRC_TABLE_LEN;i++) {
			Crc_CaculateByteCRC64NA_ViaBit(0,i,(CRC64NA_Table+i));
		}
		firstCall = FALSE;
	}
#		endif
	*crcResult = init;
#		if(CRC64NA_WIDTH>=BYTE_WIDTH)
	crcH = *crcResult>>(CRC64NA_WIDTH - BYTE_WIDTH);
#		else
	crcH = *crcResult<<(BYTE_WIDTH - CRC64NA_WIDTH);
#		endif

	*crcResult <<= BYTE_WIDTH;
	*crcResult^=CRC64NA_Table[crcH^data];
}
#	endif

void Crc_CaculateCRC64NA(CRC_T64 init, uint8 *data, uint32 len, CRC_T64 *crcResult, uint8 firstCall)
{
	uint32 i=0;
	uint8 temData=0;
	if(TRUE == firstCall) {
		*crcResult = init;
	} else {
		*crcResult = init ^ CRC64NA_XORVALUE;
#	if (STD_ON == CRC64NA_OUT_REFLECT)
		Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T64));
#	endif
	}

	for(i=0;i<len;i++){
#	if (STD_ON == CRC64NA_IN_REFLECT)
		temData = Crc_RefelctByte(data[i]);
#	else
		temData = data[i];
#	endif
#	if (STD_ON == CRC64NA_TABLE_USED)
		Crc_CaculateByteCRC64NA_ViaTable(*crcResult,temData,crcResult);
#	else
		Crc_CaculateByteCRC64NA_ViaBit(*crcResult,temData,crcResult);
#	endif
	}
	*crcResult ^= CRC64NA_XORVALUE;

#	if (STD_ON == CRC64NA_OUT_REFLECT)
	Crc_RefelctArray((uint8 *)crcResult,sizeof(CRC_T64));
#	endif
}

#	if (STD_ON == CRC64NA_CHECK_ENABLE)
void Crc_CaculateCRC64NA_Check(CRC_T64 init, CRC_T64 *crcCheck)
{
	uint8 checkArray[9] = {ASSII_1,ASSII_2,ASSII_3,ASSII_4,ASSII_5,ASSII_6,ASSII_7,ASSII_8,ASSII_9};

	Crc_CaculateCRC64NA(init,checkArray,9,crcCheck,TRUE);
}
#	endif

#	if(CRC64NA_WIDTH>=BYTE_WIDTH)
void Crc_CaculateCRC64NA_MagicCheck(CRC_T64 init,  uint8 *data, uint32 len, CRC_T64 *crcMigicCheck)
{
	uint32 i=0;
	uint8 newlen=sizeof(CRC_T64);
	uint8 newArr[sizeof(CRC_T64)];
	Crc_CaculateCRC64NA(init,data,len,crcMigicCheck,TRUE);
	for(i=0;i<newlen;i++) {
#		if (STD_ON == CRC64NA_OUT_REFLECT)
		newArr[i] = *((uint8 *)crcMigicCheck + i);
#		else
		newArr[i] = *((uint8 *)crcMigicCheck + newlen-1-i);
#		endif
	}
	Crc_CaculateCRC64NA(*crcMigicCheck,newArr,newlen,crcMigicCheck,FALSE);
	*crcMigicCheck ^= CRC64NA_XORVALUE;
}
#	endif

#endif
