/*******************************************************************************
*
* $HeadURL: http://cnshsvn001/kostal/cn_ae/GEELY/GE12/P07690_7.2KW_OBCPDU/SYS/Appl/trunk/Source/ASW/OBC/ApplDriver/CrcCaculate/CrcConfig.h $
* $Id: CrcConfig.h 53247 2017-06-23 05:17:02Z zhang135 $
* $Revision: 53247 $
*
* @file   CrcConfig.h
* @date   16.06.2017
* @author zhang135
*
******************************************************************************/
#ifndef CRC_CONFIG_H
#define CRC_CONFIG_H

#include "Std_Types.h"

#ifndef TRUE
#	define TRUE 	1
#endif
#ifndef FALSE
#	define FALSE 	0
#endif

#ifndef STD_ON
#	define STD_ON 	1
#endif
#ifndef STD_OFF
#	define STD_OFF 	0
#endif

#define CRC8SAE_ASR					STD_OFF
#define CRC8H2F_ASR					STD_OFF
#define CRC16FL_ASR					STD_ON
#define CRC32NA_ASR					STD_OFF
#define CRC32P4_ASR					STD_OFF
#ifdef	PLATFORM_64BIT_SUPPORT
#define CRC64NA_ASR					STD_OFF
#endif

#if (STD_OFF == (CRC8SAE_ASR|CRC8H2F_ASR|CRC16FL_ASR|CRC32NA_ASR|CRC32P4_ASR|CRC64NA_ASR))
#	error "At least one type CRC should be defined"
#endif

#if (STD_ON == CRC8SAE_ASR)
#	define CRC8SAE_TABLE_USED      	STD_ON
#	define CRC8SAE_CHECK_ENABLE		STD_ON
#endif

#if (STD_ON == CRC8H2F_ASR)
#define CRC8H2F_TABLE_USED      	STD_ON
#define CRC8H2F_CHECK_ENABLE		STD_ON
#endif

#if (STD_ON == CRC16FL_ASR)
#define CRC16FL_TABLE_USED      	STD_ON
#define CRC16FL_CHECK_ENABLE		STD_ON
#endif

#if (STD_ON == CRC32NA_ASR)
#define CRC32NA_TABLE_USED      	STD_ON
#define CRC32NA_CHECK_ENABLE		STD_ON
#endif

#if (STD_ON == CRC32P4_ASR)
#define CRC32P4_TABLE_USED      	STD_ON
#define CRC32P4_CHECK_ENABLE		STD_ON
#endif

#if (STD_ON == CRC64NA_ASR)
#define CRC64NA_TABLE_USED      	STD_ON
#define CRC64NA_CHECK_ENABLE		STD_ON
#endif

#endif
