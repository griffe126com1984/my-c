
#ifndef STD_TYPES_H
# define STD_TYPES_H

#include "type.h"


# define STD_HIGH     1u /* Physical state 5V or 3.3V */
# define STD_LOW      0u /* Physical state 0V */

# define STD_ACTIVE   1u /* Logical state active */
# define STD_IDLE     0u /* Logical state idle */

# define STD_ON       1u
# define STD_OFF      0u


 
#endif  /* STD_TYPES_H */

