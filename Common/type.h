#ifndef TYPE_H
#define TYPE_H

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef signed short sint16;
typedef unsigned int uint32;
typedef uint8 boolean;

#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef TRUE
#define TRUE ((boolean)1)
#endif

#ifndef FALSE
#define FALSE ((boolean)0)
#endif

#endif